---
Title: GEM Hazard Models
slug: index
...


GEM Hazard Models



Models list:

- [GEM Hazard Mosaic](mosaic)
- [Alaska](ALS)
- [Arabian Peninsula](ARB)
- [Australia](AUS)
- [Canada](CAN)
- [Caribbean and Central America](CCA)
- [Central Asia](CEA)
- [China](CHN)
- [Europe](EUR)
- [Hawai'i](HAW)
- [Indonesia](IDN)
- [India](IND)
- [Japan](JPN)
- [Mexico](MEX)
- [North Africa](NAF)
- [Northeast Asia](NEA)
- [Northwest Asia](NWA)
- [Pacific Islands](PAC)
- [Philippines](PHL)
- [Papua New Guinea](PNG)
- [South America](SAM)
- [Southeast Asia](SEA)
- [Sub-Saharan Africa](SSA)
- [Taiwan](TWN)
- [New Zealand](NZL)
- [United States of America](USA)
- [Western Africa](WAF)
- [South Africa](ZAF)


[IND]: {filename}/models/ind.md
[SEA]: {filename}/models/sea.md
[ZAF]: {filename}/models/zaf.md
[mosaic]: {filename}/models/mosaic.md
[SAM]: {filename}/models/sam.md
[AUS]: {filename}/models/aus.md
[TWN]: {filename}/models/twn.md
[PHL]: {filename}/models/phl.md
[IDN]: {filename}/models/idn.md
[HAW]: {filename}/models/haw.md
[NZL]: {filename}/models/nzl.md
[CHN]: {filename}/models/chn.md
[ARB]: {filename}/models/arb.md
[WAF]: {filename}/models/waf.md
[USA]: {filename}/models/usa.md
[SSA]: {filename}/models/ssa.md
[PNG]: {filename}/models/png.md
[PAC]: {filename}/models/pac.md
[NWA]: {filename}/models/nwa.md
[NEA]: {filename}/models/nea.md
[NAF]: {filename}/models/naf.md
[MEX]: {filename}/models/mex.md
[JPN]: {filename}/models/jpn.md
[EUR]: {filename}/models/eur.md
[CEA]: {filename}/models/cea.md
[CCA]: {filename}/models/cca.md
[CAN]: {filename}/models/can.md
[ALS]: {filename}/models/als.md

