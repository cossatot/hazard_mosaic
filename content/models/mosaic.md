---
Title: The mosaic
Date: 2018-07-03 01:30pm
Category: Models
Tags: seismic hazard, psha
Summary: The mosaic approach to global seismic hazard modelling
...


The first global seismic hazard model was released at the end of the 1990's by Giardini et al. *(1999)
