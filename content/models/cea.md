---
title: Central Asia (CEA)
slug: CEA
...

## Regional Tectonics

# OpenQuake implementation of the CEA seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

### Model

### Seismic Source Model

### Ground Motion Model


