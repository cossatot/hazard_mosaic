---
title: Alaska (ALS)
slug: ALS
...

### Regional Tectonics

Alaska sits at the northwestern edge of the North American continent. Active
faulting in the region is related to the interaction of the neighboring Pacific
and Eurasian plates with North America. The Pacific plate subducts underneath
central and western Alaska at the Aleutian subduction zone; plate convergence is
about 60 mm/yr at the eastern end of the subduction zone and 80 mm/yr in the
west. The 1964 Gulf of Alaska earthquake has been estimated at $M_w$ 9.2, one of
the largest earthquakes in history; this event occurred on the Aleutian
megathrust. Relative motion between the Pacific and North American plates is
oblique, and the substantial strike-slip component is taken up on faults in the
interior of Alaska; this slip is mostly localized on the right-lateral Denali
fault system, which hosted the 2001 $M_w$ 7.9 Denali earthquake. The Denali
fault slips about 10 mm/yr. Additional faults in the upper plate of the
subduction zone, such as the Fairweather Fault and the Chugach-St. Elias thrust
belt, may be important sources of earthuakes in the relatively populated south
of Alaska. However, active faults relating to the relative motions of Eurasia
and the hypothesized 'Bering microplate' in western Alaska are distributed
throughout the state and pose some hazard.

*Source: Yeats, 2012. Active Faults of the World. Cambridge University Press.*


# OpenQuake implementation of the USGS Alaska 2007 Seismic Hazard Model

### Model Source

### Model Repository

## Basic Datasets
