---
title: India (IND)
slug: IND
...

## Regional Tectonics

The Indian subcontinent moves northward with respect to Eurasia and is colliding
with the southern Asian margin at 35-45 mm/yr; this collision has contributed
greatly to the uplift of the great mountain ranges of central and eastern Asia,
including the Himalaya, the Tien Shan, the Pamir, and the Tibetan plateau. The
Indian subcontinent is much stronger than the Asian continental crust with which
it collides, though, so most of the deformation and seismicity that results from
the plate collision is located in and north of the Himalaya outside of India.
Exceptions include in northwest and northeast India where the Himalaya are
within the national borders, and the Shillong region of eastern India, where
some component of the plate convergence is accommodated on intraplate faults.
The faults in the Himalayan belt take up at least half of the total plate
convergence and represent the greatest source of seismic hazard to India;
earthquakes on the Himalayan thrusts may be large enough that those in Nepal,
Bhutan and Pakistan may still produce dangerous seismic shaking in
densely-populated northern India.

Nonetheless, the strong Indian crust is capable of transmitting compressive
stresses over great distances, and large, damaging earthquakes have occurred
well within the Indian borders as a result. A prime example of this is the 2001
*M<sub>w</sub>* 7.6 Bhuj earthquake in Gujarat, which killed over 2000 people
(*Bodin and Horton, 2004, Bulletin of the Seismological Society of America*)

# OpenQuake implementation of the Indian seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

### Model

### Seismic Source Model

### Ground Motion Model

