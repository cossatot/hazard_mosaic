---
title: Northwest Asia (NWA)
slug: NWA
...

## Regional Tectonics

# OpenQuake implementation of the NWA seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


