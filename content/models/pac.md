---
title: Southern Pacific Islands
slug: PAC
...

## Regional tectonics
The southern Pacific Islands region is tectonically complex and seismically very active. Since 1900, ~350 earthquakes *M*>7.0 have occurred, of which 11 were *M*>8. The greatest hazard posed by these earthquakes is triggered tsunamis, however, past events have also caused shaking related damage and fatalities. 

Most of the regional seismic hazard is attributable to interface and intraslab earthquakes along the >6000 km of subduction zones. Along the ~north-south trending Kermadec and Tonga trenches, the Pacific plate subducts beneath the Australian plate, converging at an increasing rate from ~80 mm/yr in the south to ~220 mm/yr in the north (Bird, 2003). At the point of peak convergence – the northern tip of the Tonga Trench – the plate boundary rotates counterclockwise to approximately parallel the plate motion. West of here, along a semi-continuous network of three trenches, the Australian plate subducts beneath the Pacific plate. Convergence rates range from ~35-120 mm/yr on the New Hebrides trench (Calmant 2003); ~100 mm/yr on the South Solomon trench (Wallace); and ~50-130 mm/yr on the New Britain trench (Bird, 2003). 

In addition to subduction hazards, seismicity occurs in the rapidly deforming Fiji Platform due to back arc spreading and clockwise rotation along left-lateral fracture zones (e.g. Rahiman, 2009). Some large earthquakes (M>7) also occur in the outer rise, and there is widespread distributed shallow seismicity. 


## Basic Datasets

### Earthquake Catalog
We use the magnitude-homogonized ISC-GEM extended catalogue of Weatherill et al. (2016) clipped to the Pacific Islands region (bounds of 45°S, 4°N, 145°E, and 160°W). The catalogue includes ~110,000 earthquakes *M*>2.82 from 1900-2014. 


### Fault Database
We use the GEM Active Faults Database, which includes mostly oceanic structures (spreading ridges and transform faults), but also the Fiji Fracture zone and the main frontal thrust of the New Hebridez subduction zone Back Arc Thrust Belt. 

## Model

### Seismic Source Model

The source model includes varying source typologies for the different tectonic settings. These include:

 * Interface seisimicity with *M*>6 modeled as complex faults
 * Instraslab seismicity with *M*>6 and depth < 300 km modeled as nonparametric ruptures
 * Active shallow crustal faults producing earthquakes *M*>6.5 modeled as simple fault sources
 * Distributed active shallow seismicity modeled as a grid of point sources

The interface and intraslab geometries are built using the GEM Subduction Toolkit [insert a hyperlink when this is ready]. 

The occurrence rates were determined using the following methodologies, which vary by source typology. For rates derived from seismicity, we use subcatalogues that include only the seismicity for the respective tectonic settings, declustered using Urhammer () windowing and filtered for completeness.

#### Interface

We segment each subduction zone accodring to past megathrust earthquakes, current seismicity patterns, trench convergence rates and kinematics, and assistance from thorough structural and tectonic regional studies, and assume that ruptures do not propagate across the defined boundaries. The trench segments from east to west are:

 * *New Britain*: unsegmented
 * *South Solomon*: Three segments based on the supersegments of Chen et al. (2011), which uses seafloor geomorphology, seismicity patterns, and uplift patterns from coral reefs.
 * *New Hebrides*: Four segments based on Baillard et al. (2015), depending much on varying convergence rates. Along one segment, the convergence transfers mostly to the backarc thrust belt.
 * *Kermadec-Tonga*: Three segments, loosely based on Bonnardot et al. (2007). Convergence rate of the segments decreases from north to south, and interface seismicity drastically decreases within the central segment, which encompasses the bouyant Louisville Seamount Chain.

We derive a magnitude-frequency distribution (MFD) for each interface segment using a hybrid approach that combines statistics from observed seismicity with a characteristic component derived from tectonics. The statistial approach solves for a classical Gutenberg-Richter distribution (a negative exponential) to the segment subcatalogue, as in Weichert (1980). The latter approach derives a double truncated Gaussian distribution to model the largest earthquake the segment can support (e.g., the characteristic earthquake) from fault area, convergence rate, and a seismic coupling coefficient. We compute *M*{\_max} from Thingbaijam and Mai (2017). Source characteristics with references are summarized in Table \ref{table}.

#### Intrslab
In our source model, segmentation boundaries from the interface extend into the slab, and are not meant to suggest barrier to rupture within the downgoing slab volume, but instead to allow spatial variability in observed seimsicity while still using non-parametric ruptures.

For each slab segment, we compute a single Gutenberg-Richter MFD following (Weichert, 1980) from the slab segment subcatalogues, assuming constant rates throughout each segment (Table \ref{}). We then model a set of normal slip type non-parametric rupture sources with *M*6.5-*M*{\_max,obs}+0.5, dipping 45° and 135° within slab volume of 70 km thickness. 

|<60%>|
| **Subduction zone**  | **Segment** | **a-Value** | **b-Value** | **Mw-Max** ||
| New Britain 	       | 1           |  5.373      | 0.934       | 7.50       |
|                      |             |             |             |            |
| South Solomon        | 1           |  4.116      | 0.898       | 6.95       |
|                      | 2           |  5.804      | 1.190       | 6.47       |
|                      | 3a          |  4.284      | 0.882       | 6.33       |
|                      | 3b          |  2.852      | 0.564       | 7.70       |
|                      |             |             |             |            |
| New Hebrides         | 1           |  1.875      | 0.484       | 7.00       |
|                      | 2           |  3.796      | 0.662       | 7.85       |
|                      | 3           |  4.515      | 0.792       | 7.70       |
|                      | 4           |  5.046      | 0.850       | 7.90       |
|                      |             |             |             |            |
| Kermadec-Tonga       | 1           |  5.019      | 0.864       | 7.44       |
|                      | 2           |  3.643      | 0.731       | 7.84       |
|                      | 3           |  6.711      | 1.096       | 7.80       ||

### Ground Motion Model

### References
