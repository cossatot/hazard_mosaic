---
title: North Africa (NAF)
slug: NAF
...

## Regional Tectonics

# OpenQuake implementation of the NAF seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


