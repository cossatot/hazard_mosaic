---
title: Australia (AUS)
slug: AUS
...

## Regional tectonics

Most of the Australian continent is stable, and seismicity is relatively
infrequent though intraplate earthquakes do occur and may be damaging.

The major exception to this is in far northern Australia, where the continental
crust is colliding with the microcontinents of Indonesia, particularly in Papua
New Guinea. For more information on this region, see
[Indonesia](../models/IDN/). Australian territory may also be affected
by the interaction of the Australian and Pacific plates to the east in [New
Zealand](../models/NZL/).

# OpenQuake implementation of the AUS seismic hazard model

