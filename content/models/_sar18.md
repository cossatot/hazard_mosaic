---
Title: South America Seismic Hazard Model 2018
Date: 2018-07-03 01:30pm
Category: Models
Tags: south america, seismic hazard, psha
Summary: Description of the seismic hazard model for South America
...

# South America Seismic Hazard Model (2018)
AA

## Basic Datasets

### Earthquake Catalogue

### Fault Database

## Model
AA

### Seismic Source Model
AA

### Ground Motion Model
BB

### Ground Motion Model
