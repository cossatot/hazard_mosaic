---
Title: United States National Seismic Hazard Model 2014
Date: 2018-07-03 02:30pm
Category: Models
Tags: united states of america, seismic hazard, psha
Summary: Description of the OpenQuake implementation of the seismic hazard model for the United States of America
...

# OpenQuake implementation of the United States National Seismic Hazard Model (2018)
AA

## Basic Datasets

### Earthquake Catalogue

### Fault Database

## Model
AA

### Seismic Source Model
AA

### Ground Motion Model
BB

### Ground Motion Model
