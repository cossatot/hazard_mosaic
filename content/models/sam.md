---
title: South America (SAM)
slug: SAM
...

## Regional Tectonics

Seismicity in South America primarily results from the subduction of oceanic
lithosphere beneath the western margin of the continent and from the eastward
translation of the Caribbean plate along the continent's northern margin. In the
west, the Nazca plate subducts beneath the central and northern part of the
continent, while the Antarctic plate subducts beneath the southern part of the
continent. This subduction occurs at the Nazca Trench, and has produced several
of the largest earthquakes ever recorded, including the the 1960 *M<sub>w</sub>*
9.4 Valdivia, Chile earthquake and the 2010 *M<sub>w</sub>* 8.8 Maule, Chile
earthquake. This subduction has also caused substantial deformation of the upper
plate as well, creating the Andes mountains. This deformation is ongoing and
distributed throughout the Andean zone, and may be mostly accommodated on large,
rapidly-slipping thrust faults in the Subandean Zone at the eastern rangefront
of the Andes. However, oblique subduction and gravitational forces result in
strike-slip and normal faulting throughout much of the Andes as well.

The Caribbean plate dips south beneath northern South America, though
convergence rates are low; this plate boundary may be best characterized as
strike-slip, with important left-lateral faults located on the densely-populated
Caribbean coast of Colombia and Venezuela. Offshore eastern Venezuela, the
Caribbean plate terminates and Caribbean-South American relative motion is
accommodated on the Lesser Antilles subduction zone in the Atlantic Ocean.

In many locations, the subducting oceanic slabs produce frequent earthquakes at
depths of 100 km and in some instances much deeper. Though the seismic energy
from these earthquakes is attenuated by the time it reaches the earth's surface,
the earthquakes may be of sufficient magnitude to still cause damage.

To the east and south of the Andes, the South American continent may be
considered stable. Nonetheless, like many stable continental regions, faults do
exist--many of these date to the assembly and breakup of the supercontinent
Pangea hundreds of millions of years ago. Stresses transmitted through the cold,
strong South American shield produce some earthquakes in Brazil and the
surrounding nations, though the rates of seismicity in central and eastern South
America is a small fraction of the western and northern active margins.



# OpenQuake implementation of the SARA seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

### Model

### Seismic Source Model

### Ground Motion Model


