---
title: South Africa (ZAF)
slug: ZAF
...

## Regional Tectonics

South Africa sits at the southern, passive margin of the African continent where
it transitions into oceanic crust. No plate boundaries are present in the region
and any deformation is slow. Nonetheless, there is non-negligible low to
moderate magnitude earthquake production and some paleoseismic evidence for
Holocene faulting in the Cape Fold Belt (*e.g., Goedhart and Booth, 2009*).

# OpenQuake implementation of the ZAF seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

### Model

### Seismic Source Model

### Ground Motion Model


