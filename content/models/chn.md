---
title: China (CHN)
slug: CHN
...

## Regional Tectonics

# OpenQuake implementation of the CHN seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


