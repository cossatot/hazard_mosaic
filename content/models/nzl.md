---
title: New Zealand (NZL)
slug: NZL
...

## Regional Tectonics

# OpenQuake implementation of the NZL seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


