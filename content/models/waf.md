---
title: Western Africa (WAF)
slug: WAF
...

## Regional Tectonics

# OpenQuake implementation of the WAF seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


