---
title: Europe (EUR)
slug: EUR
...

## Regional Tectonics

# OpenQuake implementation of the EUR seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


