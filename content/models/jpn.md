---
title: Japan (JPN)
slug: JPN
...

## Regional Tectonics

# OpenQuake implementation of the JPN seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


