---
Title: Australia National Hazard Model 2018
Date: 2018-07-03 03:30pm
Category: Models
Tags: Alaska, seismic hazard, psha
Summary: Description of the 2018 version of the national seismic hazard model for Australia
...

# Australia National Seismic Hazard Model (2018)
AA

## Basic Datasets

### Earthquake Catalogue

### Fault Database

## Model
AA

### Seismic Source Model
AA

### Ground Motion Model
BB

### Hazard Results
