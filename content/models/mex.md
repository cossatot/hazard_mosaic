---
title: Mexico (MEX)
slug: MEX
...

## Regional Tectonics

# OpenQuake implementation of the MEX seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


