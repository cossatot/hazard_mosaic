---
title: Sub-Saharan Africa (SSA)
slug: SSA
...

## Regional Tectonics

# OpenQuake implementation of the SSA seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


