---
title: Papua New Guinea (PNG)
slug: PNG
...

## Regional Tectonics

# OpenQuake implementation of the PNG seismic hazard model

### Model Source

### Model Repository

## Basic Datasets

### Earthquake Catalog

### Fault Database

## Model

### Seismic Source Model

### Ground Motion Model


