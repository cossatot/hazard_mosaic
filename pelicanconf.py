#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'GEM Hazard'
SITENAME = 'GEM Global Mosaic of Hazard Models'
SITEURL = ''
PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

ARTICLE_DIR = 'blog'
ARTICLE_URL = 'blog/{slug}.html'
ARTICLE_SAVE_AS = 'blog/{slug}.html'

PAGE_PATHS = ['pages', 'models']


DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORFIES_ON_MENU = True
USE_FOLDER_AS_CATEGORY = True
MENUITEMS = (
                ('Models', './pages/'),
#                ('Blog', './blog')
            )


PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

#STATIC_URL = '{path}'


AUTHOR_SAVE_AS = False
#CATEGORY_SAVE_AS = 'blog/category/{slug}.html'
#CATEGORY_URL = 'blog/category/{slug}.html'
#TAG_SAVE_AS = 'blog/tag/{slug}.html'
#TAG_URL = 'blog/tag/{slug}.html'
#DIRECT_TEMPLATES = [
#                     'blog'
                     #'blog/tags', 
                     #'blog/categories', 
#                     'archives',
                     #'blog/index'
#                    ]
#PAGINATED_DIRECT_TEMPLATES = [
    #'index', 
#    'blog/index'
#    ]


THEME = './themes/middle'


# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)


PLUGIN_PATHS = ['./plugins',
                ]

PLUGINS = [
	'pelican_jsmath',
    #'pelican-page-hierarchy',
    ]



DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
